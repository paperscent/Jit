package de.oth.jit.MerkleTree;

public interface Node {
    public String getHash();
    public void write();
}
