package de.oth.jit.MerkleTree;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StagingArea {

    public static void addFile(Path file) {
        Directory root = StagingArea.getRoot();

        Directory currentDirectory = root;
        Path currentPath = file.subpath(0, 1);
        int i = 1;
        boolean fileAdded = false;

        while (!fileAdded) {
            if (!currentDirectory.containsPath(currentPath)) {
                if (Files.isRegularFile(currentPath)) {
                    currentDirectory.addBlob(currentPath);
                    fileAdded = true;
                } else if (Files.isDirectory(currentPath)) {
                    currentDirectory.addDir(currentPath, currentDirectory);
                    currentPath = file.subpath(0, ++i);
                    currentDirectory = currentDirectory.getDirectory(file.subpath(0, i - 1));
                }
            } else if (currentDirectory.containsPath(currentPath)) {
                if (Files.isRegularFile(currentPath)) {
                    currentDirectory.removeBlob(currentPath.getFileName().toString());
                    currentDirectory.addBlob(currentPath);
                    fileAdded = true;
                } else if (Files.isDirectory(currentPath)) {
                    currentPath = file.subpath(0, ++i);
                    currentDirectory = currentDirectory.getDirectory(file.subpath(0, i - 1));
                }
            }
        }

        serialize(root);
    }

    public static void removeFile(Path file) {
        Directory root = StagingArea.getRoot();

        Path currentPath = file.subpath(0, 1);
        Directory currentDirectory = root;
        int i = 1;

        boolean fileRemoved = false;

        while (!fileRemoved) {
            if (currentDirectory.containsPath(currentPath) && Files.isRegularFile(currentPath)) {
                currentDirectory.removeBlob(currentPath.getFileName().toString());
                fileRemoved = true;

                while (currentDirectory.getParent() != null && currentDirectory.isEmpty()) {
                    currentDirectory.getParent().removeDir(currentDirectory);
                    currentDirectory = currentDirectory.getParent();
                }
            } else if (currentDirectory.containsPath(currentPath) && Files.isDirectory(currentPath)) {
                currentPath = file.subpath(0, ++i);
                currentDirectory = currentDirectory.getDirectory(file.subpath(0, i - 1));
            } else {
                System.err.println("File is not in staging area, nothing to remove.");
                System.exit(1);
            }
        }

        serialize(root);
    }

    private static Directory getRoot() {
        Directory root = null;

        if (!Files.exists(Paths.get(".jit", "staging", "staging.ser"))) {
            root = new Directory(Paths.get("."), null);
        } else {
            root = deserialize();
        }

        return root;
    }

    private static Directory deserialize() {
        Directory dir = null;

        try {
            ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(Paths.get(".jit", "staging", "staging.ser").toFile())));
            dir = (Directory) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return dir;

    }

    private static void serialize(Directory rootDirectory) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Paths.get(".jit", "staging", "staging.ser").toFile()));
            out.writeObject(rootDirectory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void commit(String commitMessage) {
        Commit newCommit = new Commit(commitMessage, getRoot());

        newCommit.write();
    }
}
