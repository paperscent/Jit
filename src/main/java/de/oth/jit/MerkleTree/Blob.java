package de.oth.jit.MerkleTree;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Blob implements Node, Serializable {

    private String hash;
    private String filename;
    private byte[] content;

    public Blob(Path file) {
        this.filename = file.getFileName().toString();
        this.hash = computeHash(file).toString();
        try {
            this.content = Files.toByteArray(file.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return filename;
    }

    private HashCode computeHash(Path file) {
        HashCode hc = null;

        try {
            hc = Files.asByteSource(file.toFile()).hash(Hashing.sha1());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return hc;
    }

    @Override
    public String getHash() {
        return this.hash;
    }

    @Override
    public void write() {
        try {
            Files.write(content, Paths.get(".jit", "objects", getHash()).toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
