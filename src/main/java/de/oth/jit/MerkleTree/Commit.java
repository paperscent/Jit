package de.oth.jit.MerkleTree;

import com.google.common.hash.Hashing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collector;

public class Commit implements Node, Serializable {

    private String commitMessage;
    private Directory root;

    public Commit(String commitMessage, Directory root) {
        this.commitMessage = commitMessage;
        this.root = root;
    }

    public String writeToObjectsFile() {
        return commitMessage + System.getProperty("line.separator") + "Directory " + root.getHash();
    }

    public static Path getRootFileFromCommitFile(Path commitFile) {
        String hashOfRoot = null;
        try {
            String directoryLine = Files.lines(commitFile).toArray(String[]::new)[1];
            String[] parts = directoryLine.split(" ");
            hashOfRoot = parts[1];
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return Paths.get(".jit", "objects", hashOfRoot);
    }

    @Override
    public String getHash() {
        return Hashing.sha1().newHasher().putString(this.writeToObjectsFile(), Charset.defaultCharset())
                .hash().toString();
    }

    @Override
    public void write() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(Paths.get(".jit", "objects", getHash()).toFile()));
            writer.write(writeToObjectsFile());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        root.write();
    }
}
