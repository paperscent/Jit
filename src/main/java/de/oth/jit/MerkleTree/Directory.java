package de.oth.jit.MerkleTree;

import com.google.common.hash.Hashing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Directory implements Node, Serializable {

    private String path;
    private Directory parent;
    private List<Blob> blobs;
    private List<Directory> directories;

    public Directory(Path directory, Directory parent) {
        this.parent = parent;
        this.path = directory.toString();
        this.blobs = new ArrayList<>();
        this.directories = new ArrayList<>();
    }

    /**
     * Add a new Blob (file) to the Directory.
     * @param file
     */
    public void addBlob(Path file) {
        blobs.add(new Blob(file));
    }

    public void removeBlob(String filename) {
        Iterator<Blob> iter = blobs.iterator();

        while (iter.hasNext()) {
            Blob current = iter.next();

            if (current.getFilename().equals(filename)) {
                blobs.remove(current);
                break;
            }
        }
    }

    public String getPath() {
        return path.toString();
    }

    public Directory getParent() {
        return parent;
    }

    /**
     * Add a new directory to the Directory.
     * @param dir
     */
    public void addDir(Path dir, Directory parent) {
        directories.add(new Directory(dir, parent));
    }

    public void removeDir(Directory dir) {
        directories.remove(dir);
    }

    public Directory getDirectory(Path path) {
        Directory dir = null;

        for (Directory directory : directories) {
            if (directory.path.equals(path.toString())) {
                dir = directory;
            }
        }

        return dir;
    }

    @Override
    public String toString() {
        String lineSeparator = System.getProperty("line.separator");
        StringBuilder output = new StringBuilder(path.toString());

        blobs.forEach(blob -> output.append(lineSeparator + "(F) " + blob.toString()));
        directories.forEach(dir -> output.append(lineSeparator + "(D) " + dir.toString()));

        return output.toString();
    }

    /**
     * This method checks if a directory or file is already part of a directory (so if it's in 'blobs' or
     * 'directories'.
     * @param path The path to a directory or file
     */
    public boolean containsPath(Path path) {
        if (Files.isDirectory(path) && directories.stream().anyMatch(dir -> dir.getPath().equals(path.toString()))) {
            return true;
        } else if (Files.isRegularFile(path) && blobs.stream().anyMatch(blob -> blob.getFilename().equals(path.getFileName().toString()))) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isEmpty() {
        return blobs.size() == 0 && directories.size() == 0;
    }

    private String writeToObjectsFile() {
        StringBuilder output = new StringBuilder("Directory" + System.getProperty("line.separator"));

        for (Directory dir : directories) {
            output.append("Directory ").append(dir.getHash()).append(" ").append(Paths.get(dir.getPath()).toFile().getName()).append(System.getProperty("line.separator"));
        }

        for (Blob b : blobs) {
            output.append("File ").append(b.getHash()).append(" ").append(b.getFilename());
        }

        return output.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Directory directory = (Directory) o;

        return path != null ? path.equals(directory.path) : directory.path == null;
    }

    @Override
    public int hashCode() {
        return path != null ? path.hashCode() : 0;
    }

    @Override
    public String getHash() {
        return Hashing.sha1().newHasher().putString(this.writeToObjectsFile(), Charset.defaultCharset())
                .hash().toString();
    }

    @Override
    public void write() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(Paths.get(".jit", "objects", getHash()).toFile()));
            writer.write(writeToObjectsFile());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Directory dir : directories) {
            dir.write();
        }

        for (Blob b : blobs) {
            b.write();
        }
    }
}

