package de.oth.jit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Utility {

    /**
     * This method is used for the 'checkout' command and creates the files from the objects files.
     * @param directoryFile The path to the directory File, e.g. ".jit/objects/a3f...23d"
     * @param directoryPath The folder in which the directoryFile is part of
     */
    public static void createFiles(Path directoryFile, Path directoryPath) throws IOException {
        // Value e.g. "A.java", Key is the Path of the objects file of "A.java"
        Map<String, Path> filesToCreate = new HashMap<>();

        Files.lines(directoryFile).filter(line -> line.startsWith("File")).forEach(file -> {
            String[] line = file.split(" ");
            filesToCreate.put(line[2], Paths.get(".jit", "objects", line[1]));
        });

        // Copy/Create the files to working directory
        for (Map.Entry<String, Path> file : filesToCreate.entrySet()) {
            if (!Files.exists(directoryPath)) {
                // When we want to create e.g. "a/b/c/d/A.java" and folders for A.java do not exist
                // yet, we have to make sure that the directories are created before. Otherwise,
                // it's not possible to create the file.
                directoryPath.toFile().mkdirs();
            }
            Files.copy(file.getValue(), directoryPath.resolve(file.getKey()));
        }

        // For the directories in the directory call the method recursively
        Files.lines(directoryFile).skip(1).filter(line -> line.startsWith("Directory")).forEach(dir -> {
            String[] line = dir.split(" ");
            try {
                createFiles(Paths.get(".jit", "objects", line[1]), directoryPath.resolve(line[2]));
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        });
    }
}
