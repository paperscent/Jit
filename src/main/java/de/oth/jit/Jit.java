package de.oth.jit;

import de.oth.jit.MerkleTree.Commit;
import de.oth.jit.MerkleTree.Directory;
import de.oth.jit.MerkleTree.StagingArea;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class Jit {

    /**
     * The entry point for Jit. This main method just checks if the command parameters are valid.
     * If so, delegate to the appropriate methods, otherwise print an error message to stderr and exit.
     */
    public static void main(String[] args) throws IOException {

        final String command = args.length >= 1 ? args[0] : null;
        final String parameter = args.length == 2 ? args[1] : null;

        if (command == null) {
            System.err.println("Command missing.");
            System.exit(1);
        } else if (args.length > 2) {
            System.err.println("This program only allows one parameter.");
            System.exit(1);
        } else if (command.equals("init") && parameter != null) {
            System.err.println("Init command does not support additional parameters.");
            System.exit(1);
        } else if (parameter == null && !(command.equals("init"))) {
            System.err.println("Invalid command.");
            System.exit(1);
        } else if (!Files.exists(Paths.get(".jit")) && !command.equals("init")) {
            System.err.println("You have to initialize your repository first.");
            System.exit(1);
        }

        switch (command) {
            case "init":
                init();
                break;
            case "add":
                add(parameter);
                break;
            case "remove":
                remove(parameter);
                break;
            case "commit":
                commit(parameter);
                break;
            case "checkout":
                checkout(parameter);
                break;
            default:
                System.err.println("Command not supported.");
                System.exit(1);
        }
    }

    /**
     * Create the hidden .jit directory with the 'objects' and 'staging' subdirectories in current working directory.
     */
    public static void init() throws IOException {
        final Path hiddenJitDirectory;

        if (Files.exists(Paths.get(".jit"))) {
            System.err.println("Repository already initialized.");
        } else {
            hiddenJitDirectory = Files.createDirectories(Paths.get(".jit"));
            Files.createDirectories(hiddenJitDirectory.resolve("objects"));
            Files.createDirectories(hiddenJitDirectory.resolve("staging"));
        }
    }

    /**
     * If the file to add exists, it is added to the staging area represented by a Merkle tree.
     *
     * @param relativePath The path to the file
     */
    public static void add(String relativePath) {
        if (!Files.exists(Paths.get(relativePath))) {
            System.err.println("File does not exist.");
            System.exit(1);
        } else {
            Path file = Paths.get(relativePath);
            StagingArea.addFile(file);
        }
    }

    /**
     * Remove a file from the staging area.
     * @param relativePath
     */
    public static void remove(String relativePath) {
        StagingArea.removeFile(Paths.get(relativePath));
    }

    /**
     * Create snapshot of current staging area.
     * @param commitMessage
     */
    public static void commit(String commitMessage) {
        StagingArea.commit(commitMessage);
    }

    /**
     * Load the tree of a commit back to working directory.
     * @param commitHash
     */
    public static void checkout(String commitHash) throws IOException {
        Path commitFile = Paths.get(".jit", "objects", commitHash);
        if (Files.exists(commitFile)) {
            Path rootFile = Commit.getRootFileFromCommitFile(commitFile);

            // Clean working directory (clean all files and directories recursively)
            Files.walk(Paths.get(""))
                    // Do not delete the .jit directory and the working directory itself
                    .filter(path -> !path.equals(Paths.get("")) && !path.startsWith(Paths.get(".jit")))
                    .map(Path::toFile)
                    // All files have to be deleted before we can delete folders
                    .sorted(Comparator.reverseOrder())
                    .forEach(File::delete);

            Utility.createFiles(rootFile, Paths.get("").toRealPath());
        } else {
            System.err.println("This commit does not exist.");
            System.exit(1);
        }
    }
}
