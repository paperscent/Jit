# Jit

Jit project for the course "Advanced Java programming" at OTH Regensburg.

If you want to test the program, use the provided `jit.jar` file.

Examples:

### Initialization
```
java -jar /path/to/jit.jar init
```

### Adding
```
java -jar /path/to/jit.jar add A.java
```